'use strict'

//
// https://stackoverflow.com/questions/16334440/is-circular-reference-between-objects-a-bad-practice#16334690
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Memory_Management#Cycles_are_not_a_problem_anymore
//

class CipherInterface 
{
    constructor(mode) {
        this.mode = mode;
        mode.cipher = this;
    }

    encrypt(plaintext, key) {
        throw console.error("encrypt not implemented");
    }

    decrypt(ciphertext, key) {
        throw console.error("decrypt not implemented");
    }

    encryptBlock(plaintext, key) {
        throw console.error("encryptBlock not implemented");
    }
    decryptBlock(ciphertext, key) {
        throw console.error("decryptBlock not implemented");
    }
}

module.exports = {
    CipherInterface
}
