'use strict'

class OpModeInterface
{
    constructor() {
        this.cipher = undefined;
    }

    encrypt(plaintext, key) {
        throw console.error("encrypt not implemented");
    }

    decrypt(ciphertext, key) {
        throw console.error("decrypt not implemented");
    }
}

function xor_eq(a, b) {
    for(let i = 0; i < a.length && i < b.length; i++)
        a[i] ^= b[i];
}

class ECB extends OpModeInterface
{
    encrypt(plaintext, key)
    {
        var enc = Buffer.alloc(0);

        for(let i = 0; i < plaintext.length; i += 16)
        {
            let p = plaintext.subarray(i, i+16);
            let res = this.cipher.encryptBlock(p, key);
            enc = Buffer.concat([enc, res]);
        }

        return enc;
    }

    decrypt(ciphertext, key)
    {
        var dec = Buffer.alloc(0);

        for(let i = 0; i < ciphertext.length; i += 16)
        {
            let c = ciphertext.subarray(i, i+16);
            let res = this.cipher.decryptBlock(c, key);
            dec = Buffer.concat([dec, res]);
        }

        return dec;
    }
};

class CBC extends OpModeInterface
{    
    encrypt(plaintext, key, IV)
    {
        var enc = Buffer.alloc(0);

        var next = IV;

        for(let i = 0; i < plaintext.length; i += 16)
        {
            let p = plaintext.subarray(i, i+16);
            
            xor_eq(p, next);

            next = this.cipher.encryptBlock(p, key);
            enc = Buffer.concat([enc, next]);
        }

        return enc;
    }

    decrypt(ciphertext, key, IV)
    {
        var dec = Buffer.alloc(0);

        var next = IV;

        for(let i = 0; i < ciphertext.length; i += 16)
        {
            let c = ciphertext.subarray(i, i+16);
            let res = this.cipher.decryptBlock(c, key);
            
            xor_eq(res, next);
            
            dec = Buffer.concat([dec, res]);
            next = c;
        }

        return  dec;
    }
}

class CFB extends OpModeInterface 
{
    encrypt(plaintext, key, IV)
    {
        var enc = Buffer.alloc(0);

        var next = IV;

        for(let i = 0; i < plaintext.length; i += 16)
        {
            let res = this.cipher.encryptBlock(next, key);

            let p = plaintext.subarray(i, i+16);

            xor_eq(res, p);

            enc = Buffer.concat([enc, res]);
            next = res;
        }

        return enc;
    }

    decrypt(ciphertext, key, IV)
    {
        var dec = Buffer.alloc(0);

        var next = IV;

        for(let i = 0; i < ciphertext.length; i+=16)
        {
            let res = this.cipher.encryptBlock(next, key);

            let c = ciphertext.subarray(i, i+16);

            xor_eq(res, c);

            dec = Buffer.concat([dec, res]);
            next = c;
        }

        return dec;
    }
}

class OFB extends OpModeInterface
{
    encrypt(plaintext, key, IV)
    {
        var enc = Buffer.alloc(0);

        var next = IV;

        for(let i = 0; i < plaintext.length; i += 16)
        {
            let res = this.cipher.encryptBlock(next, key);
            
            let p = plaintext.subarray(i, i+16);

            xor_eq(res, p);
        
            enc = Buffer.concat([enc, res], res.length+enc.length);
        }

        return enc;
    }

    decrypt(ciphertext, key, IV)
    {
        var dec = Buffer.alloc(0);

        var next = IV;

        for(let i = 0; i < ciphertext.length; i += 16)
        {
            let res = this.cipher.encryptBlock(next, key);
            
            let c = ciphertext.subarray(i, i+16);

            xor_eq(res, c);

            dec = Buffer.concat([dec, res], dec.length+res.length);
        }

        return dec;
    }
}

class CTR extends OpModeInterface
{
    encrypt(plaintext, key, nonce)
    {
        var enc = Buffer.alloc(0);
        var counter = 0;

        var next = nonce;

        for(let i = 0; i < plaintext.length; i += 16)
        {
            let res = this.cipher.encryptBlock(next, key);

            let p = plaintext.subarray(i, i+16);

            xor_eq(res, p);

            enc = Buffer.concat([enc, res]);

            counter++;
            next += counter;
        }

        return enc;
    }
    
    decrypt(ciphertext, key, nonce)
    {
        var dec = Buffer.alloc(0);
        var counter = 0;

        var next = nonce;

        for(let i = 0; i < ciphertext.length; i += 16)
        {
            let res = this.cipher.encryptBlock(next, key);

            let c = ciphertext.subarray(i, i+16);

            xor_eq(res, c);

            dec = Buffer.concat([dec, res]);
            
            counter++;            
            next += counter;
        }

        return dec;
    }

}

module.exports = {
    ECB,CBC,CFB,OFB,CTR
}