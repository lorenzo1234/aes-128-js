#!/bin/sh

test_dir="test_input"

mkdir $test_dir

dd bs=1000 count=1 </dev/urandom > $test_dir/test_input1KB
dd bs=1000 count=100 </dev/urandom > $test_dir/test_input100KB
dd bs=1000 count=1000 </dev/urandom > $test_dir/test_input1MB

