'use strict'

const AES = require('./AES.js');
const OpMode = require('./OpMode.js');
const crypto = require('crypto');
const fs = require('fs');

const {
    performance,
    PerformanceObserver
  } = require('perf_hooks');

function padPKCS7(buf)
{
    let npad = 16 - ((buf.length) % 16);
    return Buffer.concat([buf, Buffer.from(String.fromCharCode(npad).repeat(npad))], buf.length+npad);
}

function unpadPKCS7(s)
{
    let npad = s[s.length-1];
    return s.subarray(0, s.length-npad);
}

function readTestFile(filename)
{
    var fd = fs.openSync(filename);

    var test_input = Buffer.alloc(0);

    do
    {
        var buffer = Buffer.alloc(16384);
        var bytesRead = fs.readSync(fd, buffer);
        if(bytesRead <= 0) break;
        test_input = Buffer.concat([test_input, buffer], test_input.length+bytesRead);
    } while(true)

    return test_input;
}

function computeThroughput(t, nbytes)
{
    let timeSeconds = (t/1000);
    return (nbytes*8)/timeSeconds;
}

function singleTest(input, mode)
{   
    var key = crypto.randomBytes(16); 
    var IV = crypto.randomBytes(16);
    
    var aes = new AES.AES(key, mode);
    
    const enc_t0 = performance.now();
    let res = aes.encrypt(padPKCS7(input), IV);
    const enc_t1 = performance.now();
    
    const dec_t0 = performance.now();
    res = unpadPKCS7(aes.decrypt(res, IV));
    const dec_t1 = performance.now();

    fs.writeFile("./out"+mode.constructor.name, res, function (err) {
        if (err) return console.log(err);
      });

    const enc_t = enc_t1 - enc_t0;
    const dec_t = dec_t1 - dec_t0;

    let encThroughput = computeThroughput(enc_t, input.length*8);
    let decThroughput = computeThroughput(dec_t, input.length*8);

    console.log(`encrypt throughput: ${encThroughput}, time: ${enc_t1-enc_t0}`);
    console.log(`decrypt throughput: ${decThroughput}, time: ${dec_t1-dec_t0}`);

    if(Buffer.compare(res, input) != 0)
    {
        console.log("mode:", mode);
        console.log("key:", key);   
        console.log("input:", input);
        console.log("output:", res);
        return false;
    }

    return enc_t+dec_t;
}

function completeTest(input)
{
    console.log("\nECB");
    var mode = new OpMode.ECB();
    singleTest(input, mode);    

    console.log("\nCBC");
    var mode = new OpMode.CBC();
    singleTest(input, mode);

    console.log("\nOFB");
    var mode = new OpMode.OFB();
    singleTest(input, mode);
}

var test_input1KB = readTestFile('./test_input/test_input1KB');
completeTest(test_input1KB);

var test_input100KB = readTestFile('./test_input/test_input100KB');
completeTest(test_input100KB);

var test_input1MB = readTestFile('./test_input/test_input1MB');
completeTest(test_input1MB);
